
#### 介绍
简单的代码生成工具，不限语言，目前支持sqlserver和mysql。代码很简单可以自己修改满足需求，使用ado.net访问数据库，使用正则表达式查找替换。
<br /><br />
![图片介绍](https://gitee.com/kerwincui/kwcode/raw/master/1.png)
<br /><br />


#### 使用说明

```
把示例模板template文件夹复制到应用程序目录即可使用

config.xml       数据库连接配置文件
template文件夹    模板文件
generator-code   生成的文件


模板文件中替换字符
	$TableName       首字母大写表名（$TableNames是复数形式）
	$tableName       首字母小写表名（$tableNames是复数形式）	
	$table-name      中划线分隔的小写表名
	$CNName          中文表名称
	$TableShortName  删除名称中的Data/Schedule/Base	 		


表字段循环   
	$foreach 
	${[NotMap(Id,user_name)]       //[NotMap(,,)]要排除的字段集合,可选
		$TableColumnName       //字段名
		$TableColumnCNName     //字段说明
		$TableColumnType       //字段类型
	$}


表循环 
	$foreach(table)            //table-循环表   
	${
    		$TableMapName     //字段名
	$}

```



