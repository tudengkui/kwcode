﻿using AutoMapper;
using JTTMS.App.$TableNames.Dto;
using JTTMS.THEntity.$TableNames;
using JTTMS.Utilty;
using System;
using System.Collections.Generic;
using System.Text;
using static JTTMS.JTTMSEnum.JTTMSEnum;

namespace JTTMS.App.$TableNames.Mapper
{
    public class $TableNameMapper : Profile
    {
        // <summary>
        /// 映射
        /// </summary>
        public $TableNameMapper()
        {
            CreateMap<$TableName, $TableNameOutput > ();
            CreateMap<$TableNameEditInput, $TableName > ();
            CreateMap<$TableNameCreateInput, $TableName > ();
        }
    }
}
