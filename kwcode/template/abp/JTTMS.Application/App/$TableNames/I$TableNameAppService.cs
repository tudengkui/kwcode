﻿using Abp.Application.Services.Dto;
using JTTMS.App.$TableNames.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace JTTMS.App.$TableNames
{
    /// <summary>
    /// 车辆管理接口
    /// </summary>
     public interface I$TableNameAppService:IAsyncJTTMSCrudAppService<$TableNameOutput, int, $TableNameSearchInput, $TableNameCreateInput, $TableNameEditInput, EntityDto> {}

}
