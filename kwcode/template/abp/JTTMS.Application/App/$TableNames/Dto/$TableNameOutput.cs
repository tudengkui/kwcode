﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using JTTMS.App.Drivers.Dto;
using JTTMS.App.WlhyTypes.Dto;
using JTTMS.THEntity.$TableNames;
using JTTMS.Utilty;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static JTTMS.JTTMSEnum.JTTMSEnum;

namespace JTTMS.App.$TableNames.Dto
{
    /// <summary>
    /// $CNName输出模型
    /// </summary>
    [AutoMapFrom(typeof($TableName))]
    public class $TableNameOutput : EntityDto<int>
    {
        
    }
}
